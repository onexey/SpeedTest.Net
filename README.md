# SpeedTest.Net

A .NET core class library fork of NSpeedTest, a C# client library to test internet bandwidth using speedtest.net.
Based on [SpeedTest.Net](https://github.com/JoyMoe/SpeedTest.Net)

Curently targeted for .NET core 3.1

Changed a library and test method to make it possible target .NET Framework and .NET Standard.
